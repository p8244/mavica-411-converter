"""
Run this file in a direction with python ./mavica411converter.py to convert all .411 files to .png files.

By CrossedOmega
2024/05/17
"""

# Why do I spend time on this?


def mavica_411_to_yuv_arrays(fp):
    """
    Takes in a 411 file pointer and returns the Y,U,V and combined YCbCr arrays as a length 4 tuple.
    A 411 thumbnail has a fixed size of 64 by 48 pixels. The YUV arrays have length 3072, the YCbCr 3072*3.
    Mavica systems use a weird byte packing that differs from the normal 411 UV subsampling (V4L2_PIX_FMT_Y41P ('Y41P')).
    For 0x00 to 0x05:
    Y0; Y1; Y2; Y3; Cb0-3; Cr0-3
    This fills the first four pixels:
    Y0,Cb0-3,Cr0-3;  Y1,Cb0-3,Cr0-3;  Y2,Cb0-3,Cr0-3;  Y3,Cb0-3,Cr0-3
    """
    
    import numpy as np
    from PIL import Image

    # total number of bytes: 4608
    imagesize = (48,64)
    img_bytes = 64*48 # 3072, one layer of bytes
    #Cb_bytes = Cr_bytes = (4608-3072)//2
    chunksize = 6
    chunks = 4608//chunksize #768

    npimg = np.fromfile(fp, dtype='B')
    #6 Y Y Y Y Cb Cr
    #Y = 0,1,2,3, Cb = 4, Cr = 5
    Y = np.zeros(img_bytes, 'B')
    Cb = np.zeros(img_bytes, 'B')
    Cr = np.zeros(img_bytes, 'B')
    YCbCr = np.zeros(img_bytes*3, 'B')

    npimg = npimg.reshape((chunks,chunksize))
    for i,v in enumerate(npimg):
        #print(v)
        Y[i*4:(i+1)*4]  = [v[0],v[1],v[2],v[3]]*1
        Cb[i*4:(i+1)*4] =                [v[4]]*4 # here's the 411 subsampling
        Cr[i*4:(i+1)*4] =                [v[5]]*4
        YCbCr[i*12:(i+1)*12] = [v[0],v[4],v[5],
                                v[1],v[4],v[5],
                                v[2],v[4],v[5],
                                v[3],v[4],v[5],]

    Y  = Y.reshape(imagesize)
    Cb = Cb.reshape(imagesize)    
    Cr = Cr.reshape(imagesize)
    YCbCr = YCbCr.reshape((48,64,3))
    return Y,Cb,Cr,YCbCr


def yuv_array_to_Image(YUV_array):
    """
    PIL has inbuilt YUV conversion. Be careful that there are different ways YUV to RGB can be done, especially considering the u8 to floating point and clamping operations mess with the colors a lot. 
    Take the first line from the conversion if you want the YCbCr PIL Image.
    """
    
    from PIL import Image
    
    ycbcr_img = Image.fromarray(YUV_array.astype('uint8'), 'YCbCr')
    rgb_img = ycbcr_img.convert('RGB')
    return rgb_img


if __name__ == '__main__':
    import glob
    from PIL import Image
    for file in glob.glob("*.411"):
        Y,Cb,Cr,YCbCr = mavica_411_to_yuv_arrays(file)
        rgb = yuv_array_to_Image(YCbCr)
        rgb.save(file.replace('.411','.png'))