# Mavica 411 Converter


## Description
Converts Sony Mavica .411 files to png.

## Installation
Install Python. Install PIL and Numpy. Place mavica411converter.py in a folder where you want to convert your .411 files.

## Usage
Run the Python script with ```python mavica411converter.py``` in your favorite shell. Converts all .411 files to png with the same stem filename.

## Support
None. Pull requests are open.

## Roadmap
None.

## License
No licence yet.